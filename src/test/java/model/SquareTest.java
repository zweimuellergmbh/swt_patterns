package model;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

public class SquareTest {

    @Test
    public void square_returns_square_of_input() {
        Square cs = new Square();
        assertThat(cs.getSquare(4)).isEqualTo(16);
    }
}