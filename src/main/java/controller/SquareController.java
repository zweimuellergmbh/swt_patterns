package controller;

import model.Square;
import org.eclipse.swt.SWT;
import view.SquareView;

public class SquareController {

    private Square square;
    private SquareView squareView;

    public SquareController(Square square, SquareView squareView) {
        this.square = square;
        this.squareView = squareView;

        squareView.getCalcButton().addListener(SWT.Selection, e -> onCalculate());
    }

    private void onCalculate() {
        int toSquare = 0;
        try {
            toSquare = Integer.parseInt(squareView.getInputField().getText());
        } catch (Exception e) {
            // return
        }

        squareView.getOutputField().setText(String.valueOf(square.getSquare(toSquare)));
    }
}
