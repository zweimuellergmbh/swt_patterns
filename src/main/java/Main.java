import controller.SquareController;
import model.Square;
import org.eclipse.swt.widgets.Display;
import view.SquareView;
import view.SquareViewImpl;

public class Main {

    public static void main(String[] args) {
        Display display = new Display();

        final SquareView squareView = new SquareViewImpl(display);
        final Square square = new Square();

        new SquareController(square, squareView);

        squareView.show();

//        Shell shell = new Shell(display);
//        shell.setSize(100,100);
//        shell.setLayout(new RowLayout());
//
//
//        final Button button = new Button(shell, SWT.PUSH);
//        button.setText("Click Me");
//
//        final Text text = new Text(shell, SWT.SHADOW_IN);
//
//        button.addSelectionListener(new SelectionListener() {
//
//            public void widgetSelected(SelectionEvent event) {
//                text.setText("No worries!");
//            }
//
//            public void widgetDefaultSelected(SelectionEvent event) {
//                text.setText("No worries!");
//            }
//        });
//
//        shell.open();
//        while (!shell.isDisposed()) {
//            if (!display.readAndDispatch())
//                display.sleep();
//        }
//        display.dispose();
    }
}