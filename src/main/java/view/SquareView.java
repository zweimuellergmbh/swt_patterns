package view;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

public interface SquareView {
    void show();

    Text getOutputField();

    Button getCalcButton();

    Text getInputField();
}
