package view;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class SquareViewImpl extends Shell implements SquareView {

    private final Display display;

    private Button calcButton;
    private Text input;
    private Text output;

    public SquareViewImpl(final Display display) {
        super(display, SWT.SHELL_TRIM);
        this.display = display;

        createContents();
    }

    private void createContents() {
        setText("SWT Application");
        setSize(200, 200);
        setLayout(new GridLayout(1, true));

        input = new Text(this, SWT.SHADOW_IN);

        calcButton = new Button(this, SWT.PUSH);
        calcButton.setText("Calculate Square");

        output = new Text(this, SWT.SHADOW_IN);
    }

    @Override
    public Text getInputField() {
        return input;
    }

    @Override
    public Text getOutputField() {
        return output;
    }

    @Override
    public Button getCalcButton() {
        return calcButton;
    }

    @Override
    protected void checkSubclass() {
        // Disable the check that prevents subclassing of SWT components
    }

    @Override
    public void show() {
        try {
            open();
            layout();
            while (!isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}